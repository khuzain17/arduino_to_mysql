#include <SPI.h>
#include <Ethernet.h>
#include <Wire.h> 
#include <Adafruit_MLX90614.h>

Adafruit_MLX90614 mlx = Adafruit_MLX90614();
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };

float DataKelembaban;
float DataSuhu;

int period = 3000;
unsigned long time_now = 0;

unsigned long byteCount = 0;
bool printWebData = true;

int port = 8080;


char server[] = "192.168.1.177";
IPAddress ip(192,168,1,175);
EthernetClient client; 

void setup() {
  Serial.begin(115200);

  mlx.begin(); 
  
  Ethernet.begin(mac, ip);
  Serial.print("Local IP: ");
  Serial.println(Ethernet.localIP());
  delay(2000);
}

void SendtoDB(){
   if (client.connect(server, port)) {
    Serial.println("");
    Serial.println("connected");
    // Make a HTTP request:
    Serial.print("GET /arduino_mysql/control.php?dataKelembaban=");
    Serial.print(DataKelembaban);
    Serial.print("&dataSuhu=");
    Serial.println(DataSuhu);
    Serial.println("");
    
    client.print("GET /arduino_mysql/control.php?dataKelembaban=");     //YOUR URL
    client.print(DataKelembaban);
    client.print("&dataSuhu=");
    client.print(DataSuhu);
    client.print(" ");      //SPACE BEFORE HTTP/1.1
    client.print("HTTP/1.1");
    client.println();
    client.println("Host: 192.168.1.175");
    client.println("Connection: close");
    client.println();
  } else {
    
    Serial.println("connection failed");
  }
 }

void loop(){
  DataKelembaban = mlx.readAmbientTempC();
  DataSuhu = mlx.readObjectTempC(); 
  String print_temp = "Temp: ";  print_temp += String(DataSuhu); print_temp += " C";
  String print_humd = "Humd: ";  print_humd += String(DataKelembaban); print_humd += " %";

  if(millis() >= time_now + period){
    time_now += period;
    SendtoDB();
  } 
  
  int len = client.available();
  if (len > 0) {
        byte buffer[80];
        if (len > 80) len = 80;
        client.read(buffer, len);
        if (printWebData) {
          Serial.write(buffer, len); 
        }
        byteCount = byteCount + len;
  } 
  
}
